use clap::{Parser, Subcommand};
use log::debug;
use std::fs::{DirBuilder, File};
use std::io::ErrorKind;
use std::process::Command;

mod config;
mod docker;

#[derive(Parser)]
#[clap(author, version, about, long_about=None)]
struct Cli {
    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// generate .devenv/config.yaml file
    Init {
        /// base image for container/custom image
        #[clap(value_parser)]
        base: Option<String>,
        /// Use the recommended .devenv/config.yaml
        #[clap(short, long, action)]
        default: bool,
    },
    /// Build custom image at .devenv/Dockerfile (require .devenv/config.yaml image section)
    Build,
}

fn main() {
    env_logger::init();
    let cli = Cli::parse();

    // You can check for the existence of subcommands, and if found use their
    // matches just as you would the top level cmd
    match &cli.command {
        Commands::Init { base, default } => {
            if default.to_owned() {
                let config = config::Config::default(base.to_owned());
                config.write_file().unwrap();
                debug!("{:#?}", config);
            } else {
                let config = config::Config::minimal(base.to_owned());
                config.write_file().unwrap();
                debug!("{:#?}", config);
            }
            println!(
                "'devenv init' was used, base is: {:?}, default is: {:?}",
                base, default
            )
        }
        Commands::Build => {
            let current_dir = String::from(std::env::current_dir().unwrap().to_str().unwrap());
            let file_path = format!("{}/.devenv/config.yaml", current_dir);

            let file_exists = std::path::Path::new(file_path.as_str()).is_file();
            if file_exists {
                let config = config::Config::from_file(file_path).unwrap();
                let dockerfile = docker::Dockerfile::from_config(config);
                dockerfile.write_file().unwrap();
                debug!("Dockerfile:\n{:#?}", dockerfile);
            } else {
                println!(".devenv/config.yaml file doesn't exists! Use devenv init to create one!");
            }
            println!("'devenv build' was used")
        }
    }

    // let mut editors: HashMap<&str, &str> = HashMap::new();
    // editors.insert("neovim", "nvim");
    // editors.insert("vim", "vim");
    // editors.insert("emacs", "emacs");
    // editors.insert("vscode", "code");
    // editors.insert("vscodium", "code");

    // let mut df = docker::Dockerfile::new("ubuntu:22.04".to_string());
    // df.set_pre_config();
    // df.set_user_setup("dev".to_string(), "dev".to_string(), "zsh".to_string());

    // let current_dir = String::from(std::env::current_dir().unwrap().to_str().unwrap());
    // let devenv_dir_path = format!("{}/{}", current_dir, ".devenv");
    // let devenv_config_path = format!("{}/{}", devenv_dir_path, "config.yaml");
    // let config = config::Config::from_file(devenv_config_path);
    //
    // let df = docker::Dockerfile::from_config(config.unwrap());
    //
    // df.write_file().unwrap();
    // df.build();

    // terminal_routine();
    // docker_routine();
}

fn terminal_routine() {
    let terminal_name = "kitty";
    let shell_name = "zsh";
    let mut terminal = Command::new(terminal_name);

    let _open_terminal = terminal
        .args([
            "--hold", // keep open
            shell_name,
            "-c",
            "sudo docker exec -ti dev zsh",
        ])
        .spawn()
        .expect(format!("{} not found!", terminal_name).as_str());
}

fn docker_routine() {
    let current_dir = String::from(std::env::current_dir().unwrap().to_str().unwrap());
    // println!("current dir: {}", current_dir);

    let devenv_dir_path = format!("{}/{}", current_dir, ".devenv");
    let devenv_dir_exists = std::path::Path::new(devenv_dir_path.as_str()).is_dir();

    if devenv_dir_exists {
        let devenv_config_path = format!("{}/{}", devenv_dir_path, "config.yaml");
        let devenv_config_exists = std::path::Path::new(devenv_config_path.as_str()).is_file();

        if devenv_config_exists {

            // let config_file = std::fs::read_to_string(devenv_config_path).unwrap();

            // let config = YamlLoader::load_from_str(&config_file).expect("Yaml syntax error!");
            //
            // println!("file: \n{:?}", config);
            //
            // let container_name = &config[0]["container"]["name"];
            //
            // docker_check();
            //
            // docker_start_container(container_name.as_str().unwrap());
            //
            // // docker_open_container(container_name.as_str().unwrap());
            //
            // docker_stop_container(container_name.as_str().unwrap());
        } else {
            println!(".devenv/config.yaml file doesn't exist yet");

            let devenv_config =
                File::create(devenv_config_path).expect("couldn't create .devenv/config.yaml file");
        }
    } else {
        println!(".devenv folder doesn't exist yet");

        let devenv_dir_builder = DirBuilder::new()
            .create(devenv_dir_path)
            .expect("couldn't create .devenv folder");
    }
}

fn docker_check() {
    println!("Trying to exec a docker ps ...");
    let docker_check = Command::new("docker").arg("ps").output();
    match docker_check {
        Err(e) => {
            let kind = e.kind();
            match kind {
                ErrorKind::NotFound => {
                    panic!("`docker` wasn't found! Check your PATH!");
                }
                _ => panic!("Unknown docker error: {}", kind.to_string()),
            }
        }
        Ok(out) => {
            let msg = String::from_utf8(out.stderr).unwrap();
            if msg.contains("Got permission denied") {
                panic!("You are not in docker group: {}", msg);
            }
        }
    };
}

fn docker_start_container(container_name: &str) {
    println!("Starting container {}...", container_name);
    let mut docker = Command::new("docker");

    // Start container
    let command = "start";
    docker
        .args([command, container_name])
        .output()
        .expect(format!("container {} not found!", container_name).as_str());
}

fn docker_open_container(container_name: &str) {
    // Start editor inside the container
    let command = "exec";
    let entrypoint = "zsh";
    let mut docker = Command::new("docker");
    let mut child = docker
        .args([command, "-ti", container_name, entrypoint])
        .spawn()
        .expect(format!("container {} isn't running!", container_name).as_str());

    child
        .wait()
        .expect(format!("container {} isn't running!", container_name).as_str());
    println!("Exiting container {}...", container_name);
}

fn docker_stop_container(container_name: &str) {
    println!("Stopping container {}...", container_name);
    let mut docker = Command::new("docker");

    // Stop container after exit
    let command = "stop";
    docker
        .args([command, container_name])
        .output()
        .expect(format!("container {} isn't running!", container_name).as_str());
}
