use std::{
    fs::File,
    io::{self, Write},
    process::Command,
};

use log::{debug, error};

use crate::config::{Config, Image};

struct OS {
    name: String,
    install_cmd: String,
    cleanup_cmp: String,
    basic_dependencies: Vec<String>,
    gui_dependencies: Vec<String>,
}

impl OS {
    fn new() -> OS {
        OS {
            name: String::new(),
            install_cmd: String::new(),
            cleanup_cmp: String::new(),
            basic_dependencies: Vec::new(),
            gui_dependencies: Vec::new(),
        }
    }

    fn get_os(name: String) -> OS {
        if name.contains("ubuntu") {
            return OS::ubuntu();
        }
        if name.contains("arch") {
            return OS::arch();
        }

        error!("Unsupported base os: {}", name);
        panic!("Unsupported base os: {}", name);
    }

    fn ubuntu() -> OS {
        OS {
            name: String::from("ubuntu"),
            install_cmd: String::from(
                "apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends",
            ),
            cleanup_cmp: String::from("apt-get clean && rm -rf /var/lib/apt/lists/*"),
            basic_dependencies: vec![
                "ca-certificates".to_string(),
                "tzdata".to_string(),
                "sudo".to_string(),
                "tmux".to_string(),
                "git".to_string(),
                "curl".to_string(),
                "unzip".to_string(),
                "tar".to_string(),
                "ripgrep".to_string(),
                "xclip".to_string(),
                "gnupg".to_string(),
                "locales".to_string(),
                "build-essential".to_string(),
            ],
            gui_dependencies: vec!["xauth".to_string()],
        }
    }

    fn arch() -> OS {
        OS {
            name: String::from("archlinux"),
            install_cmd: String::from("pacman -Syyu --noconfirm"),
            cleanup_cmp: String::from("pacman -Scc --noconfirm"),
            basic_dependencies: vec![
                "ca-certificates".to_string(),
                "tzdata".to_string(),
                "sudo".to_string(),
                "tmux".to_string(),
                "git".to_string(),
                "curl".to_string(),
                "unzip".to_string(),
                "tar".to_string(),
                "ripgrep".to_string(),
                "xclip".to_string(),
                "gnupg".to_string(),
                "base-devel".to_string(),
            ],
            gui_dependencies: vec!["libxcrypt-compat".to_string(), "xorg-xauth".to_string()],
        }
    }
}

#[derive(Debug)]
pub struct Dockerfile {
    base_image: String,
    image_name: String,
    pre_config: String,
    user_setup: String,
    // TODO
    // install_cmd: String,
    default_cmd: String,
}

impl Dockerfile {
    pub fn new(base_image: String) -> Dockerfile {
        Dockerfile {
            base_image,
            image_name: String::new(),
            pre_config: String::new(),
            user_setup: String::new(),
            // TODO
            // install_cmd: String::new(),
            default_cmd: String::new(),
        }
    }

    pub fn from_config(config: Config) -> Dockerfile {
        if let Some(image) = Image::from_config(config.clone()).unwrap() {
            let mut dockerfile = Dockerfile::new(image.base);
            dockerfile.image_name = image.name.unwrap();
            dockerfile.set_pre_config(
                image.install_gui.unwrap_or(false),
                image.install.unwrap_or(Vec::new()),
            );
            dockerfile.set_user_setup(
                image.user.unwrap_or("dev".to_string()),
                image.group.unwrap_or("dev".to_string()),
                image.shell.clone(),
            );
            dockerfile.set_default_cmd(image.editor.unwrap_or(image.shell));
            return dockerfile;
        }

        panic!("Couldn't define a base image");
    }

    pub fn set_pre_config(&mut self, gui_enable: bool, additional_dependencies: Vec<String>) {
        let mut dependencies = String::new();
        let os = OS::get_os(self.base_image.clone());

        self.pre_config
            .push_str(format!("FROM {}\n", self.base_image).as_str());

        self.pre_config
            .push_str(format!("\nRUN {}", os.install_cmd).as_str());
        for dependency in os.basic_dependencies {
            dependencies.push_str(format!(" \\\n    {}", dependency).as_str());
        }
        if gui_enable {
            for dependency in os.gui_dependencies {
                dependencies.push_str(format!(" \\\n    {}", dependency).as_str());
            }
        }
        for dependency in additional_dependencies {
            dependencies.push_str(format!(" \\\n    {}", dependency).as_str());
        }

        self.pre_config.push_str(dependencies.as_str());
        if os.cleanup_cmp.len() > 0 {
            self.pre_config
                .push_str(format!(" \\\n    && {}", os.cleanup_cmp).as_str());
        }
        self.pre_config.push_str("\n");

        debug!("pre config command:\n{}", self.pre_config);
    }

    pub fn set_user_setup(&mut self, user: String, group: String, shell: String) {
        let uid = Command::new("id")
            .arg("-u")
            .output()
            .expect("command id wasn't found");

        let uid = String::from_utf8(uid.stdout).unwrap().trim().to_string();
        // debug!("User id: {}", uid);

        // let user_name = Command::new("id")
        //     .args(["-u", "-n"])
        //     .output()
        //     .expect("command id wasn't found");
        // let user_name = String::from_utf8(user_name.stdout)
        //     .unwrap()
        //     .trim()
        //     .to_string();
        // debug!("User name: {}", user_name);

        let gid = Command::new("id")
            .arg("-g")
            .output()
            .expect("command id wasn't found");

        let gid = String::from_utf8(gid.stdout).unwrap().trim().to_string();
        // debug!("User name: {}", gid);

        // let group_name = Command::new("id")
        //     .args(["-g", "-n"])
        //     .output()
        //     .expect("command id wasn't found");
        // let group_name = String::from_utf8(group_name.stdout)
        //     .unwrap()
        //     .trim()
        //     .to_string();
        // debug!("Group name: {}", group_name);

        self.user_setup
            .push_str(format!("\nRUN groupadd --gid {} {}", gid, group).as_str());

        self.user_setup.push_str(
            format!(
                " \\\n    && useradd -s $(command -v {}) --create-home --uid {} --gid {} {}",
                shell, uid, gid, user
            )
            .as_str(),
        );

        self.user_setup.push_str(
            format!(
                " \\\n    && echo \"{} ALL=(root) NOPASSWD:ALL\" > /etc/sudoers.d/{}",
                user, user
            )
            .as_str(),
        );

        self.user_setup
            .push_str(format!(" \\\n    && chmod 0440 /etc/sudoers.d/{}", user).as_str());

        self.user_setup
            .push_str(format!(" \\\n    && echo \"{}:{}\" | chpasswd", user, user).as_str());

        self.user_setup
            .push_str(format!(" \\\n    && usermod -aG audio,video {}", user).as_str());

        let full_lang = std::env::var("LANG").unwrap_or("en_US.UTF-8".to_string());
        let full_lang: Vec<&str> = full_lang.split(".").collect();
        let lang = full_lang[0];
        let charset = full_lang[1];

        self.user_setup.push_str(
            format!(
                " \\\n    && localedef -i {} -c -f {} -A /usr/share/locale/locale.alias {}.{}",
                lang, charset, lang, charset
            )
            .as_str(),
        );

        // TODO: setup timezone

        self.user_setup
            .push_str(format!("\n\nUSER {}", user).as_str());

        self.user_setup
            .push_str(format!("\n\nWORKDIR /home/{}\n", user).as_str());

        debug!("user setup command:\n{}", self.user_setup);
    }

    pub fn set_default_cmd(&mut self, cmd: String) {
        self.default_cmd = format!("\nCMD {}\n", cmd);
        debug!("default command:\n{}", self.default_cmd);
    }

    pub fn write_file(&self) -> io::Result<()> {
        let current_dir = String::from(std::env::current_dir().unwrap().to_str().unwrap());
        let file_path = format!("{}/.devenv/Dockerfile", current_dir);
        let file_exists = std::path::Path::new(file_path.as_str()).is_file();
        if file_exists {
            std::fs::remove_file(file_path.as_str())?;
        }

        let mut file = File::create(file_path.as_str())?;
        file.write_all(self.pre_config.as_bytes())?;
        file.write_all(self.user_setup.as_bytes())?;
        file.write_all(self.default_cmd.as_bytes())?;
        Ok(())
    }

    pub fn build(&self) {
        println!("Building image {}...", self.image_name);

        let current_dir = String::from(std::env::current_dir().unwrap().to_str().unwrap());
        let file_path = format!("{}/.devenv/", current_dir);

        let mut child = Command::new("docker")
            .args([
                "build",
                "--tag",
                self.image_name.as_str(),
                file_path.as_str(),
            ])
            .spawn()
            .expect(format!("file {}/Dockerfile isn't exist!", file_path).as_str());

        child
            .wait()
            .expect(format!("image {} isn't running!", self.image_name).as_str());
        println!("Image {} built!", self.image_name);
    }
}
