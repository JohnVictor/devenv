use serde::{Deserialize, Serialize};

use std::{
    error::Error,
    fs::File,
    io::{self, Write},
};

use log::{debug, error};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Config {
    pub container: Container,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub image: Option<Image>,
}

impl Config {
    pub fn new() -> Config {
        Config {
            container: Container::new(),
            image: Some(Image::new()),
        }
    }

    pub fn minimal(base: Option<String>) -> Config {
        Config {
            container: Container::minimal(base),
            image: None,
        }
    }

    pub fn default(base: Option<String>) -> Config {
        Config {
            container: Container::default(base.clone()),
            image: Some(Image::default(base.clone())),
        }
    }

    pub fn from_file(path: String) -> Result<Config, Box<dyn Error>> {
        debug!("path: {:#?}", path);
        let config_string = std::fs::read_to_string(&path)?;
        debug!("path: {:#?}", path);
        let config = serde_yaml::from_str::<Config>(&config_string)?;
        debug!("config: {:#?}", config);
        Ok(config)
    }

    pub fn write_file(&self) -> io::Result<()> {
        let current_dir = String::from(std::env::current_dir().unwrap().to_str().unwrap());
        let file_path = format!("{}/.devenv/config.yaml", current_dir);
        let file_exists = std::path::Path::new(file_path.as_str()).is_file();
        if file_exists {
            println!(".devenv/config.yml already exists!");
            return Ok(());
        }

        let value = serde_yaml::to_string(&self).unwrap();
        let mut file = File::create(file_path.as_str())?;
        file.write_all(value.as_bytes())?;
        Ok(())
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Container {
    #[serde(skip_serializing_if = "Option::is_none")]
    name: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub image: Option<String>,

    pub cmd: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub enable_gui: Option<bool>,
}

impl Container {
    pub fn new() -> Container {
        Container {
            name: Some(String::new()),
            image: Some(String::new()),
            cmd: String::new(),
            enable_gui: Some(false),
        }
    }

    pub fn minimal(base: Option<String>) -> Container {
        let mut image = String::from("ubuntu:22.04");

        if let Some(os) = base {
            if os.contains("ubuntu") || os.contains("archlinux") {
                image = os;
            } else {
                println!("Base image unsupported! The supported are ubuntu and archlinux");
                println!("Using ubuntu as default");
            }
        } else {
            println!("Base image not provided! Using ubuntu as default");
        }

        Container {
            name: None,
            image: Some(image),
            cmd: String::from("bash"),
            enable_gui: None,
        }
    }

    pub fn default(base: Option<String>) -> Container {
        // Default use a custom image
        Container {
            name: None,
            image: None,
            cmd: String::from("zsh"),
            enable_gui: None,
        }
    }

    pub fn from_file(path: String) -> Result<Container, Box<dyn Error>> {
        let config = Config::from_file(path)?;
        debug!("container: {:#?}", config.container);

        Container::from_config(config)
    }

    pub fn from_config(config: Config) -> Result<Container, Box<dyn Error>> {
        let current_dir = String::from(std::env::current_dir().unwrap().to_str().unwrap());
        let path_slices: Vec<&str> = current_dir.split("/").collect();
        let current_dir_name = path_slices.last().unwrap().to_string();
        let container_name = format!("{}-devenv", current_dir_name);

        let mut container: Container = config.container;
        container.name = Some(container.name.unwrap_or(container_name));

        if container.image.is_some() {
            return Ok(container);
        }

        if config.image.is_some() {
            let image_name = format!("{}-image", current_dir_name);
            let image_name = config.image.unwrap().name.unwrap_or(image_name);
            container.image = Some(image_name);
            return Ok(container);
        }

        panic!("Couldn't define a container image");
    }

    pub fn create(&self) {
        debug!("running docker create {}", self.name.clone().unwrap());
        let result = std::process::Command::new("docker")
            .args([
                "create",
                self.name.clone().unwrap().as_str(),
                self.image.clone().unwrap().as_str(),
            ])
            .output();

        match result {
            Err(_) => {
                error!(
                    "container {} couldn't be created!",
                    self.name.clone().unwrap()
                )
            }
            Ok(_) => {}
        }
    }

    pub fn start(&self) {
        debug!("running docker start {}", self.name.clone().unwrap());
        let result = std::process::Command::new("docker")
            .args(["start", self.name.clone().unwrap().as_str()])
            .output();

        match result {
            Err(_) => {
                error!("container {} not found!", self.name.clone().unwrap())
            }
            Ok(_) => {}
        }
    }

    pub fn stop(&self) {
        debug!("running docker stop {}", self.name.clone().unwrap());
        let result = std::process::Command::new("docker")
            .args(["stop", self.name.clone().unwrap().as_str()])
            .output();

        match result {
            Err(_) => {
                error!("container {} not found!", self.name.clone().unwrap())
            }
            Ok(_) => {}
        }
    }

    pub fn remove(&self) {
        debug!("running docker rm {}", self.name.clone().unwrap());
        let result = std::process::Command::new("docker")
            .args(["rm", self.name.clone().unwrap().as_str()])
            .output();

        match result {
            Err(_) => {
                error!("container {} not found!", self.name.clone().unwrap())
            }
            Ok(_) => {}
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Image {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    pub base: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub user: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub group: Option<String>,

    pub shell: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub editor: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub install_gui: Option<bool>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub install: Option<Vec<String>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    languages: Option<Vec<String>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    configs: Option<Vec<String>>,
}

impl Image {
    pub fn new() -> Image {
        Image {
            name: Some(String::new()),
            base: String::new(),
            user: Some(String::new()),
            group: Some(String::new()),
            shell: String::new(),
            editor: None,
            install_gui: Some(false),
            install: Some(Vec::new()),
            languages: Some(Vec::new()),
            configs: Some(Vec::new()),
        }
    }

    pub fn minimal(base: Option<String>) -> Image {
        let mut image_base = String::from("ubuntu:22.04");

        if let Some(os) = base {
            if os.contains("ubuntu") || os.contains("archlinux") {
                image_base = os;
            } else {
                println!("Base image unsupported! The supported are ubuntu and archlinux");
                println!("Using ubuntu as default");
            }
        } else {
            println!("Base image not provided! Using ubuntu as default");
        }

        Image {
            name: None,
            base: image_base,
            user: None,
            group: None,
            shell: String::from("bash"),
            editor: None,
            install_gui: None,
            install: None,
            languages: None,
            configs: None,
        }
    }

    pub fn default(base: Option<String>) -> Image {
        let mut image = Image::minimal(base);
        image.user = Some(String::from("dev"));
        image.group = Some(String::from("dev"));
        image.install = Some(vec!["zsh".to_string()]);
        image.shell = String::from("zsh");

        image
    }

    pub fn from_file(path: String) -> Result<Option<Image>, Box<dyn Error>> {
        let config = Config::from_file(path)?;
        debug!("image: {:#?}", config.image);

        Image::from_config(config)
    }

    pub fn from_config(config: Config) -> Result<Option<Image>, Box<dyn Error>> {
        if config.image.is_some() {
            let current_dir = String::from(std::env::current_dir().unwrap().to_str().unwrap());
            let path_slices: Vec<&str> = current_dir.split("/").collect();
            let current_dir_name = path_slices.last().unwrap().to_string();
            let image_name = format!("{}-image", current_dir_name);

            let mut image: Image = config.image.unwrap();
            image.name = Some(image.name.unwrap_or(image_name));
            return Ok(Some(image));
        }

        Ok(config.image)
    }
}
